pageextension 50102 "TCN_CustomerListExtPru" extends "Customer List" //MyTargetPageId
{
    layout
    {
        modify("Balance (LCY)")
        {
            Visible = xMostrarCalculados;
        }

        modify("Balance Due (LCY)")
        {
            Visible = xMostrarCalculados;
        }

        modify("Payments (LCY)")
        {
            Visible = xMostrarCalculados;
        }

        modify("Sales (LCY)")
        {
            Visible = xMostrarCalculados;
        }

        addlast(Control1)
        {
            field(ImportePedidosFacturas; cuTCN_RegistrosVarios.ImportePedidosFacturasClienteF(Rec, false))
            {
                Caption = 'Importe pedidos mas facturas';

                trigger OnDrillDown()
                begin
                    cuTCN_RegistrosVarios.ImportePedidosFacturasClienteF(Rec, True);
                end;
            }
        }
    }
    actions
    {
        addlast(Creation)
        {
            action(GetFilterNo)
            {
                Caption = 'Ejemplo GetFilter campo Nº';
                trigger OnAction()
                begin
                    Message(GetFilter("No."));
                end;
            }

            action(GetFilters)
            {
                Caption = 'Ejemplo GetFilters';
                trigger OnAction()
                begin
                    Message(GetFilters);
                end;
            }
        }

        addlast(Reporting)
        {
            action(Ocultar)
            {
                Caption = 'Ocultar campos calculados';
                Image = Balance;
                Visible = xMostrarCalculados;

                trigger OnAction()
                begin
                    xMostrarCalculados := false;
                end;

            }

            action(Mostrar)
            {
                Caption = 'Mostrar campos calculados';
                Image = Bank;
                Visible = Not xMostrarCalculados;

                trigger OnAction()
                begin
                    xMostrarCalculados := true;
                end;
            }
        }
    }

    var
        xMostrarCalculados: Boolean;
        cuTCN_RegistrosVarios: Codeunit TCN_RegistrosVarios;
}