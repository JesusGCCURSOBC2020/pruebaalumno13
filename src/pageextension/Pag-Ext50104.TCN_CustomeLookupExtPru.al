pageextension 50104 "TCN_CustomeLookupExtPru" extends "Customer Lookup" //MyTargetPageId
{
    layout
    {
        modify("Credit Limit (LCY)")
        {
            Visible = xCreditoMaximoVisible;
        }

        modify("Phone No.")
        {
            Visible = xCreditoMaximoVisible;
        }
    }

    procedure GetSelectionFilterF(var prCustomer: Record Customer)

    begin
        SetSelectionFilter(prCustomer);
    end;

    // Pregunta de examen seguro
    procedure VisibilidadCreditoMaximoVisible(pVisible: Boolean)
    var
    begin
        xCreditoMaximoVisible := pVisible;
    end;

    var
        xCreditoMaximoVisible: Boolean;
}