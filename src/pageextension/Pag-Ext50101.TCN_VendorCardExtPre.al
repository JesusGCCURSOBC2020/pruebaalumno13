pageextension 50101 "TCN_VendorCardExtPre" extends "Vendor Card"
{
    layout
    {
        modify("Balance (LCY)")
        {
            Visible = false;
        }
    }

    actions
    {

    }

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        if not Confirm('Desea salir de la pagina', true) then begin
            exit(false);
        end;
    end;
}