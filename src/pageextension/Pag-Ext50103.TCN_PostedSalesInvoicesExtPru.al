pageextension 50103 "TCN_PostedSalesInvoicesExtPru" extends "Posted Sales Invoices" //MyTargetPageId
{
    actions
    {
        addlast(Navigation)
        {
            action(PruebaImpresion)
            {
                Caption = 'Prueba e impresion';
                Image = Absence;
                trigger OnAction()
                var
                    rlTempSalesInvoiceHeader: Record "Sales Invoice Header";
                    xlFiltro: Text;
                    rlTempSalesInvoiceHeaderTmp: Record "Sales Invoice Header" temporary;
                begin

                    // Solucion mas utilizada 01 
                    rlTempSalesInvoiceHeader := rec; // Copiamos todos los campos de nuestro rec a la tabla creada
                    rlTempSalesInvoiceHeader.Find(); // Busca un registro que esta asociado a los campos de la clave
                    rlTempSalesInvoiceHeader.SetRecFilter(); // Volvemos a aplicar el filtro desde la tabla creada
                    report.Run(report::"Sales - Invoice", true, false, rlTempSalesInvoiceHeader); // Ejecutamos el filtro que hemos almacenado

                    // // Solucion 02
                    // xlFiltro:= GetView();   // Almacena el/los filtro/s que tenga la pagina
                    // Message(xlFiltro);
                    // SetRecFilter(); // Indicamos que el filtro sea unico
                    // report.Run(report::"Sales - Invoice", true, false, rlTempSalesInvoiceHeader);
                    // Reset(); // Borramos todos los filtros
                    // SetView(xlFiltro); // Me vuelve a aplicar el filtro de manera unica

                    // Solucion 03
                    // rlTempSalesInvoiceHeaderTmp.CopyFilters(Rec); // Guardamos los filtros de la pagina
                    // SetRecFilter();
                    // report.Run(report::"Sales - Invoice", true, false, rlTempSalesInvoiceHeader);
                    // Reset();
                    // CopyFilters(rlTempSalesInvoiceHeaderTmp); // Restauramos los filtros que tenia la pagina previamente
                end;
            }
        }
    }
}