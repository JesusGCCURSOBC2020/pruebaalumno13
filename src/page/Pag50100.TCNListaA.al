page 50100 "TCNListaA"
{

    PageType = List;
    SourceTable = TCN_a;
    Caption = 'TCNListaA';
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Color; Color)
                {
                    ApplicationArea = All;
                }
                field(DiaSemana; DiaSemana)
                {
                    ApplicationArea = All;
                }
                field(Talla; Talla)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(NumCliente; NumCliente)
                {
                    ApplicationArea = All;
                }
                field(NombreCliente; NombreCliente)
                {
                    ApplicationArea = All;
                }

                field(NombreClienteFuncion; cuRegistrosVarios.NombreClienteF(NumCliente))
                {
                    Caption = 'Nombre de cliente con funcion';
                    ApplicationArea = All;
                }
                field(NumPersona; NumPersona)
                {
                    ApplicationArea = All;
                }
                field(TipoPersona; TipoPersona)
                {
                    ApplicationArea = All;
                }
                field(SumaImporte; SumaImporte)
                {
                    ApplicationArea = All;
                }
                field(CodAve; CodAve)
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Creation)
        {
            action(SetValor)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.SetValorF('Hola curso');

                end;
            }

            action(GetValor)
            {
                trigger OnAction()
                begin
                    Message(cuRegistrosVarios.GetValorF());
                end;
            }

            action(EjecutarCodeUnit)
            {
                trigger OnAction()
                var
                    cuRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    i: Integer;
                    rlAveTMP: Record TCN_Ave temporary;
                begin
                    // rlAveTMP.DeleteAll(false); // Porque es temporal sino no lo fuera no hacerlo
                    for i := 1 to 10 do begin
                        // Commit();
                        if not cuRegistrosVarios.Run() then begin
                            rlAveTMP.Init();
                            rlAveTMP.Codigo := format(i);
                            rlAveTMP.Nombre := GetLastErrorText;
                            rlAveTMP.Insert(false);
                        end;
                        page.Run(0, rlAveTMP);
                        ;
                    end;
                end;
            }

            action(Sobrecarga)
            {
                trigger OnAction()
                var
                    culRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    xlBigI: BigInteger;
                    xlFecha: Date;
                    xlDateFormula: Text;

                begin
                    xlBigI := 99999;
                    xlFecha := Today;
                    xlDateFormula := '-1S';
                    Message(culRegistrosVarios.SobreCargaF(xlBigI));
                    Message(culRegistrosVarios.SobreCargaF(xlFecha));
                    Message(culRegistrosVarios.SobreCargaF(xlFecha, xlDateFormula));
                end;
            }

            action(SegundoPlano)
            {
                trigger OnAction()
                var
                    culRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    xlNumSesionIniciada: Integer;
                    xlSesionIniciada: Boolean;
                begin
                    culRegistrosVarios.EjecucionSegundoPlanoF();
                    // xlSesionIniciada := StartSession(xlNumSesionIniciada, Codeunit::TCN_RegistrosVarios);
                    // Message(format(xlNumSesionIniciada));
                end;
            }

            action(PruebasVarias)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.Run();
                end;
            }
        }
    }

    var
        cuRegistrosVarios: Codeunit TCN_RegistrosVarios;
}
