page 50109 "TCN_ListaTiposVariosPruebas"
{
    PageType = List;
    SourceTable = TCN_TiposVariosPruebas;
    Caption = 'TCN_ListaTiposVariosPruebas';
    ApplicationArea = All;
    UsageCategory = Lists;
    ShowFilter = false;
    DataCaptionExpression = xDescripcion; // Puede ser una variable, una funcion o texto.
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Codigo)
                {
                    CaptionClass = StrSubstNo('3,tipo %1s', Tipo); // Cambia el caption del codigo
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(Palmeada; Palmeada)
                {
                    ApplicationArea = All;
                    Visible = xPalmeadaVisible;
                }
            }
        }
    }

    local procedure AsignarCamposVisiblesF()
    var
        xlGrupoFiltroInicial: Integer;
        i: Integer;
    begin
        xlGrupoFiltroInicial := FilterGroup;

        for i := 0 to 10 do begin
            FilterGroup(i);
            if GetFilter(Tipo) <> '' then begin
                xDescripcion := StrSubstNo('Lista %1s', GetFilter(Tipo));
            end;

            if GetFilter(Tipo) = format(Tipo::Pata) then begin
                xPalmeadaVisible := true;
                i := 11;
            end;
            FilterGroup := xlGrupoFiltroInicial;
        end;
    end;

    trigger OnOpenPage()
    begin
        AsignarCamposVisiblesF();
    end;

    var
        xPalmeadaVisible: Boolean;
        xDescripcion: Text;
}
