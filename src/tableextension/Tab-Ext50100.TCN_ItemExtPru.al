tableextension 50100 "TCN_ItemExtPru" extends Item //MyTargetTableId
{
    fields
    {
        field(50100; "CodAlmacen2"; Code[10])
        {
            Caption = 'Codigo almacen 2';
            TableRelation = Location.Code;
        }

        Modify(Description)
        {
            trigger OnAfterValidate()
            begin
                Description := LowerCase(Description);
            end;
        }
    }

}