codeunit 50100 "TCN_RegistrosVarios"
{
    Permissions = tabledata "Sales Invoice Header" = mid,
                  tabledata "Sales Invoice Line" = mid;

    SingleInstance = true;

    var
        xValor: Text;
        xSeleccion: Integer;


    trigger OnRun()
    var
        rlCustomer: Record Customer;
        xlNumCampo: Integer;
        xlCampo: Text;
        rlTempBlob: Record TempBlob temporary;
        xl: Integer;
        xlInStream: InStream;
        ventana: Dialog;
        xlNumVeces: Integer;
        xlFichero: Text;
        xlLinea: Text;
        xlOutStream: OutStream;

    begin
        EjemploExcelF();
        // EjemploCalSumsF();
        // GrupoFiltros2F();
        // EjemploFiltrosF();
        // ManejoFicheroF();
        // ExportacionDatosF();
        // exit;
        // Error('Ejecutando Cu');
        // EjecucionSegundoPlanoF();
        // Ejemplo de confirm
        // if Confirm('¿Confirma que desea registrar el registro actual?', false) then begin
        //     Message('Registro realizado');
        // end
        // else begin
        //     Error('Proceso cancelado por el usuario');
        // end;

        // Ejemplo strMenu
        // xSeleccion := StrMenu('Enviar, Facturar, Enviar y facturar', 2, '¿Que desea hacer?');
        // Message(Format(xSeleccion));

        // Ejemplo de ventanas de progreso
        // xlNumVeces := 1000000000;

        // if GuiAllowed then begin
        //     ventana.Open('Procesando @1@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        // end;

        // for xSeleccion := 1 to xlNumVeces do begin
        //     ventana.Update(1, Round(xSeleccion / xlNumVeces * 10000, 1));
        // end;

        // if GuiAllowed then begin
        //     ventana.Close();
        // end;

        // Ejemplo inStream
        // rlTempBlob.Blob.CreateInStream(xlInStream, TextEncoding :: Windows); // Enlaza nuestro stream con nuestro blob // Con TextEncoding :: windows coge los acentos

        // if UploadIntoStream('Seleccione fichero', '', 'Archivos de texto (*.txt, *.csv)|*.txt, *.csv| todos los archivos (*.*)|*.*', xlFichero, xlInStream) then begin
        //     while not xlInStream.EOS do begin
        //         xlInStream.ReadText(xlLinea);
        //         xlNumCampo := 0;

        //         rlCustomer.Init();
        //         //Primera opcion usarlo como una lista normal con split y una variable de tipo text
        //         foreach xlCampo in xlLinea.Split(';') do begin
        //             xlNumCampo += 1;
        //             case xlNumCampo of
        //                 1:
        //                     begin
        //                         rlCustomer.Validate("No.", xlCampo);
        //                     end;

        //                 2:
        //                     begin
        //                         rlCustomer.Validate(Name, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.Name)));
        //                     end;

        //                 3:
        //                     begin
        //                         rlCustomer.Validate(Address, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.Address)));
        //                     end;

        //                 4:
        //                     begin
        //                         rlCustomer.Validate(City, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.City)));
        //                     end;
        //             end;
        //         end;
        //         rlCustomer.Insert(true);
        //     end;
        // end
        // else begin
        //     Error('No se pudo subir el fichero');
        // end;
    end;

    procedure SetValorF(pValor: Text)
    begin
        xValor := pValor;
        Message('Valor %1', xValor);
    end;

    procedure GetValorF(): Text
    begin
        exit(xValor);
    end;

    local procedure RegistrarEntrada() xSalida: Integer
    begin

    end;

    [TryFunction]
    local procedure EjemploTry()
    begin
    end;

    local procedure EjemploArray()
    var
        mlArrayEjemplo: array[100, 200] of Text;
        mlArray2: array[100, 200] of Text;
        i: Integer;
        x: Integer;
    begin
        for i := 1 to ArrayLen(mlArrayEjemplo[1]) do begin
            for x := 1 to ArrayLen(mlArrayEjemplo[2]) do begin
                mlArrayEjemplo[i, x] := 'h';
            end;
        end;

        CopyArray(mlArrayEjemplo, mlArray2, 1);
    end;

    procedure SobreCargaF(pParametro: BigInteger) xSalida: Text
    begin
        xSalida := format(pParametro + 1);
    end;

    procedure SobreCargaF(pParametro: Date) xSalida: Text
    begin
        xSalida := format(CalcDate('+1D', pParametro));
    end;

    procedure SobreCargaF(pParametro: Date; pFormulaFecha: Text) xSalida: Text
    begin
        xSalida := format(CalcDate(pFormulaFecha, pParametro));
    end;

    procedure EjecucionSegundoPlanoF()
    var
        i: Integer;
        ventana: Dialog;
        clMensaje: Label 'Sesion Segundo Plano Finalizada';
        rlAve: Record TCN_Ave;
    begin
        if GuiAllowed then begin
            ventana.Open('#1##########');
        end;

        for i := 1 to 9999999 do begin
            if GuiAllowed then begin
                ventana.Update(1, i);
            end;
        end;
        // rlAve.Init();
        // rlAve.Validate(Codigo, Format(SessionId()));
        // rlAve.Validate(Nombre, clMensaje);
        // rlAve.Insert(true);
        OnFinEjecucionSegundoPlanoF(CurrentDateTime, SessionId());

    end;

    local procedure ExportacionDatosF()
    var
        rlVendor: Record Vendor;
        rlCustomer: Record Customer;
        rlTempTempBlob: Record TempBlob temporary;
        xlOutStream: OutStream;
        xlInStream: InStream;
        xlFichero: Text;
    begin
        xlFichero := 'proveedores.txt';
        rlTempTempBlob.Blob.CreateOutStream(xlOutStream);
        rlTempTempBlob.Blob.CreateInStream(xlInStream);
        with rlVendor do begin
            if FindSet(false) then begin
                repeat
                    CalcFields("Balance (LCY)"); // Calculamos el campo calculado
                                                 //Caso 1 
                    xlOutStream.WriteText("No." + ';');
                    xlOutStream.WriteText(Name + ';');
                    xlOutStream.WriteText(Address + ';');
                    xlOutStream.WriteText(City);
                    xlOutStream.WriteText(format("Balance (LCY)"));
                    xlOutStream.WriteText(); // Damos un salto de linea

                    //Caso 2
                    xlOutStream.WriteText(StrSubstNo('%2%1%3%1%4%1%5%1%6', ';', "No.",
                                                                          Name,
                                                                          Address,
                                                                          City,
                                                                          "Balance (LCY)"));
                    xlOutStream.WriteText();
                until Next() = 0;

                CopyStream(xlOutStream, xlInStream);
                if DownloadFromStream(xlInStream, 'Fichero a Descargar', '', 'Archivos de texto (*.txt, *.csv)|*.txt, *.csv| todos los archivos (*.*)|*.*', xlFichero) then begin
                    Message('Se ha descargado el fichero %1', xlFichero);
                end
                else begin
                    Error('Se ha producido un error %1', GetLastErrorText);
                end;
            end;
        end;
    end;

    local procedure ManejoFicheroF()
    var
        rlItem: Record Item;
        rlGLAccount: Record "G/L Account";
        rlTCNa: Record TCN_a;
        xlEnum: Enum TCN_Colores;
        rlSalesLine: Record "Sales Line";
        rlSalesLine2: Record "Sales Line";
        rlSalesShipmentLine: Record "Sales Shipment Line";

    begin
        with rlItem do begin
            SetCurrentKey(Description);
            Ascending(false);
            SetRange("No.", '1000', '2000'); // Lo usamos cuando es un campo o desde un valor a otro en un campo
            SetRange("No."); // Limpia los filtros de ese campo
            SetFilter("No.", '%1', '1000');
            SetFilter("No.", '%1..%2|%3..%4', '1000', '1001', '2000', '2020');
            // SetFilter("No.", '%1',Cadena);// La primera opcion devolveria la cadena de la pagina
            // SetFilter("No.", Cadena); // La segunda opcion con * algunas veces no funciona
            SetFilter(Description, '%1', 'Bicicle*');
            ModifyAll("Unit Price", 0, true);
        end;
        rlGLAccount.SetRange("Account Type", rlGLAccount."Account Type"::Posting);
        rlTCNa.SetRange(Color, xlEnum::Rojo);

        rlSalesLine.get(rlSalesLine."Document Type"::Order, '104001', '20000');

        // rlSalesLine."Document Type" := rlSalesLine."Document Type"::Order;
        // rlSalesLine."Document No." := '104001';
        // rlSalesLine."Line No." := 20000;
        // rlSalesLine.Find();

        rlSalesLine.SetRange("Document Type", rlSalesLine."Document Type"::Order);
        rlSalesLine.SetRange("Document No.", '104001');
        rlSalesLine.SetFilter("Qty. to Ship", '<> %1', 0);

        if rlSalesLine.FindSet(true) then begin
            repeat
                // Este no funciona correctamente
                // rlSalesLine.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                // rlSalesLine.Modify(true);

                // Solucion 1
                rlSalesLine2.get(rlSalesLine."Document Type", rlSalesLine."Document No.", rlSalesLine."Line No.");
                rlSalesLine2.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                rlSalesLine2.Modify(true);

                // Solucion 2
                rlSalesLine2 := rlSalesLine;
                rlSalesLine2.Find();
                rlSalesLine2.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                rlSalesLine2.Modify(true);
            until rlSalesLine.Next() = 0;
        end;

        // 
        rlSalesShipmentLine.TransferFields(rlSalesLine, true);
        // Preguntar si tenemos permisos de lectura
        //if rlSalesLine.ReadPermission then begin end;

    end;


    [IntegrationEvent(true, true)]
    local procedure OnFinEjecucionSegundoPlanoF(pMomentoFinalizacion: DateTime; pSesionID: Integer)
    begin
    end;

    // El procedimiento se llama GrupoFiltrosF.Quitamos la variable
    local procedure EjemploFiltrosF()
    var
        rlSalesHeader: Record "Sales Header";
        xlGrupoFiltro: Integer;
    begin
        xlGrupoFiltro := rlSalesHeader.FilterGroup;
        rlSalesHeader.FilterGroup(xlGrupoFiltro + 2); // Nos cambiamos de grupo del 0 al 2
        rlSalesHeader.SetRange("Document Type", rlSalesHeader."Document Type"::Order); // Aplicamos los filtros
        rlSalesHeader.SetFilter("Sell-to Customer No.", '<>%1', ''); // Para que nos muestren los pedidos con nº de clientes
        rlSalesHeader.FilterGroup(xlGrupoFiltro - 2);// Volvemos al grupo en el que nos encontrabamos
        if Page.RunModal(0, rlSalesHeader) in [Action::LookupOK, Action::OK] then begin
            Message(rlSalesHeader."No.");
        end;
    end;

    local procedure GrupoFiltros2F()
    var
        rlCustomer: Record Customer;
        plCustomerLookup: Page "Customer Lookup";
    begin
        rlCustomer.FilterGroup(rlCustomer.FilterGroup + 2); // No poner un 1 puesto que crea un filtro global para toda la app
        rlCustomer.SetRange("Salesperson Code", 'JR'); // y es posible que no funcione correctamente
        rlCustomer.FilterGroup(rlCustomer.FilterGroup - 2);
        rlCustomer.Get('27090917');
        plCustomerLookup.SetTableView(rlCustomer);
        plCustomerLookup.LookupMode(true); // Muestra el boton de aceptar o cancelar
        plCustomerLookup.SetRecord(rlCustomer);

        if plCustomerLookup.RunModal() in [Action::LookupOK, Action::OK] then begin
            plCustomerLookup.GetRecord(rlCustomer);
            plCustomerLookup.GetSelectionFilterF(rlCustomer);

            if rlCustomer.FindSet(false) then begin
                repeat
                    Message(rlCustomer."No.");
                until rlCustomer.Next() = 0
            end;
        end;
    end;

    local procedure EjemploCalSumsF()
    var
        rlTCNA: Record TCN_a;
        rlSalesLine: Record "Sales Line";
    begin
        // Calcfields
        rlTCNA.SetRange(NumCliente, '10000');
        rlTCNA.SetFilter(FiltroFecha, '%1', DMY2Date(20, 01, 2021));
        rlTCNA.SetRange(FiltroProducto, 'LS-150');

        if rlTCNA.FindFirst() then begin
            rlTCNA.CalcFields(SumaImporte);
            Message(format(rlTCNA.SumaImporte));
        end;

        // Calcsum
        rlSalesLine.SetRange("Sell-to Customer No.", '10000');
        rlSalesLine.SetFilter("Shipment Date", '%1', DMY2Date(20, 01, 2021));
        rlSalesLine.SetRange("No.", 'LS-150');
        rlSalesLine.CalcSums(Amount);
        Message(Format(rlSalesLine.Amount));
    end;

    procedure ImportePedidosFacturasClienteF(prCustomer: Record Customer; pDrillDown: Boolean) xSalida: Decimal
    var
        rlSalesLine: Record "Sales Line";
        rlSalesInvoiceLine: Record "Sales Invoice Line";
        rlSalesLineTemp: Record "Sales Line" temporary;

    begin
        with rlSalesLine do begin
            SetRange("Sell-to Customer No.", prCustomer."No.");
            SetRange("Document Type", "Document Type"::Order);

            if pDrillDown then begin
                if FindSet(false) then begin
                    repeat
                        rlSalesLineTemp.Init();
                        rlSalesLineTemp.TransferFields(rlSalesLine);
                        rlSalesLineTemp.Insert(false);
                    until Next() = 0;
                end;
            end
            else begin
                CalcSums(Amount);
            end;
            xSalida := Amount;
        end;

        with rlSalesInvoiceLine do begin
            SetRange("Sell-to Customer No.", prCustomer."No.");

            if pDrillDown then begin
                if FindSet(false) then begin
                    repeat
                        rlSalesLineTemp.Init();
                        rlSalesLineTemp.TransferFields(rlSalesLine);
                        rlSalesLineTemp."Document Type" := rlSalesLineTemp."Document Type"::Invoice;
                        rlSalesLineTemp.Insert(false);
                    until Next() = 0;
                end;
            end
            else begin
                CalcSums(Amount);
            end;
            xSalida += Amount;

            if pDrillDown then begin
                Page.Run(0, rlSalesLineTemp);
            end;
        end;
    end;


    procedure NombreClienteF(xCodCliente: Code[20]) xSalida: Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(xCodCliente) then begin
            xSalida := rlCustomer.Name;
        end;
    end;

    local procedure EjemploExcelF()
    var
        rlExcelBufferTmp: Record "Excel Buffer" temporary;
        xlFichero: Text;
        xlInStream: InStream;
        xlHoja: Text;
        xlNombreLibro: Text;
    begin
        if UploadIntoStream('Seleccione libro excel', '', 'Archivos de excel (*.xls, *.xlsx)|*.xls, *.xlsx| todos los archivos (*.*)|*.*', xlFichero, xlInStream) then begin

            xlHoja := rlExcelBufferTmp.SelectSheetsNameStream(xlInStream);
            xlNombreLibro := rlExcelBufferTmp.OpenBookStream(xlInStream, xlHoja);
            Message(xlNombreLibro);

            rlExcelBufferTmp.ReadSheet();
            // rlExcelBufferTmp.SetFilter(rlExcelBufferTmp."Row No.",'>%1',1); // Filtramos por la fila mayor que 1
            if rlExcelBufferTmp.FindSet(false) then begin
                repeat
                    Message(StrSubstNo('Fila %1 | Columna %2 | Valor %3', rlExcelBufferTmp.xlRowID, rlExcelBufferTmp.xlColID, rlExcelBufferTmp."Cell Value as Text"));
                until rlExcelBufferTmp.Next() = 0;
            end;
        end
        else begin
            Error('No se ha podido abrir el fichero excel %1, %2', xlFichero, GetLastErrorText);
        end;
    end;


}
