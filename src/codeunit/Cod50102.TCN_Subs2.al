codeunit 50102 "TCN_Subs2"
{
    SingleInstance = true;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::TCN_RegistrosVarios, 'OnFinEjecucionSegundoPlanoF',
    '', false, false)]
    local procedure OnFinEjecucionSegundoPlanoFCodeunitTCNRegistrosVariosF(pSesionID: Integer; pMomentoFinalizacion: DateTime)
    begin
        Message('%1, %2', pSesionID, pMomentoFinalizacion);
    end;
}