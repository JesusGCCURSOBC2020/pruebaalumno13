codeunit 50101 "TCN_SuscripcionesVar"
{
    SingleInstance = true;
    // EventSubscriberInstance = Manual;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnAfterInsertEvent', '', false, false)]
    local procedure MyProcedure(var Rec: Record Customer)
    begin
        if not Confirm(StrSubstNo('¿Desea insertar el cliente %1?', Rec."No.", true)) then begin
            Error('Proceso cancelado por el usuario');
        end;
    end;


    [EventSubscriber(ObjectType::Table, Database::TCN_Lineas, 'onAfterVerificarDatosLineasF',
         '', false, false)]
    local procedure onAfterVerificarDatosLineasFTableTCNLineasF(var RecLineas: Record TCN_Lineas; xRecLineas: Record TCN_Lineas)
    begin
        if RecLineas.DesVenta = ' ' then begin
            RecLineas.DesVenta := 'Sin descripcion';
        end;

        if RecLineas.DesVenta = xRecLineas.DesVenta then begin
            RecLineas.DesVenta := 'Misma descripcion';
        end;
    end;

    /*[EventSubscriber(ObjectType::Table, Database::Item, 'OnAfterValidateEvent', 'Description', false, false)]
    local procedure OnAfterValidateEventDescriptionTableItemF(var Rec: Record Item)
    begin
        Rec.Description := UpperCase(Rec.Description);
    end;*/

    [EventSubscriber(ObjectType::Codeunit, Codeunit::TCN_RegistrosVarios, 'OnFinEjecucionSegundoPlanoF', '', false, false)]
    local procedure OnFinEjecucionSegundoPlanoFCodeunitTCNRegistrosVarios(pMomentoFinalizacion: DateTime)
    var
        clMensaje: Label 'Se ha finalizado el proceso en 2ª plano el %1';
    begin
        Message(clMensaje, pMomentoFinalizacion);
    end;


}