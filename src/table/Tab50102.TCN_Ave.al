table 50102 "TCN_Ave"
{
    Caption = 'Aves';
    LookupPageId = TCN_ListaAves;
    DrillDownPageId = TCN_ListaAves;

    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Codigo"; Code[10])
        {
            Caption = 'Codigo';
            DataClassification = ToBeClassified;
        }

        field(2; "Nombre"; Text[50])
        {
            DataClassification = ToBeClassified;
            trigger OnValidate()
            var
                rlCustumer: Record Customer;
            begin
                Message('Has insertado %1 en el registro con codigo %2', Nombre, Codigo);

                rlCustumer.Init();
                rlCustumer."No." := Codigo;
                rlCustumer.Insert();
            end;
        }

        field(3; "Preguntar"; Boolean)
        {
            trigger OnValidate()
            var
                culSuscripcionesVar: Codeunit TCN_SuscripcionesVar;
            begin
                if Preguntar then begin
                    BindSubscription(culSuscripcionesVar);
                end else begin
                    UnbindSubscription(culSuscripcionesVar);
                end;
            end;
        }

        field(4; "TipoPico"; Code[10])
        {
            Caption = 'Tipo de pico';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pico));
        }


        field(5; "TipoPluma"; Code[10])
        {
            Caption = 'Tipo de pluma';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pluma));
        }


        field(6; "TipoPata"; Code[10])
        {
            Caption = 'Tipo de pata';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pata));
        }
    }

    keys
    {
        key(PK; "Codigo")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Nombre) { }
    }
}