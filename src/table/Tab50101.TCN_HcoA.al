table 50101 "TCN_HcoA"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Color"; Enum TCN_Colores)
        {
            DataClassification = ToBeClassified;
        }

        field(2; "Talla"; Enum Tallas)
        {
            DataClassification = ToBeClassified;
        }

        field(3; "DiaSemana"; Option)
        {
            OptionMembers = Lunes,Martes,Miercoles;
        }

        field(4; "FechaRegistro"; Date)
        {
            Caption = 'Fecha registro';
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; Color)
        {
            Clustered = true;
        }
    }

}