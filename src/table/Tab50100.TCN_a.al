table 50100 "TCN_a"
{
    DataClassification = ToBeClassified;

    Caption = 'Tabla A';
    LookupPageId = TCNListaA;
    DrillDownPageId = TCNListaA;

    fields
    {
        field(1; "Color"; Enum TCN_Colores)
        {

            DataClassification = ToBeClassified;
        }

        field(2; "Talla"; Enum Tallas)
        {
            DataClassification = ToBeClassified;
        }


        field(3; "DiaSemana"; Option)
        {
            OptionMembers = Lunes,Martes,Miercoles,Jueves;
        }

        field(4; "NumCliente"; Code[20])
        {
            Caption = 'Numero cliente';
            TableRelation = Customer."No."; //where ("Phone No." = filter (<> ' '));
            DataClassification = ToBeClassified;

            trigger OnValidate()
            begin
                TestField(Talla, Talla::" XXL");
                Message('Has elegido el cliente %1', NumCliente);
            end;
        }

        field(5; "Descripcion"; Text[250])
        {
            Caption = 'Descripcion';
            DataClassification = ToBeClassified;

            trigger OnValidate()
            begin
                TestField(Talla);
                Descripcion := UpperCase(Descripcion);
            end;
        }

        field(6; "TipoPersona"; Enum TCN_TipoPersona)
        {
            Caption = 'Cliente o proveedor';
            InitValue = 'Cliente';
            DataClassification = ToBeClassified;
        }

        field(7; "NumPersona"; Code[20])
        {
            Caption = 'Nº cte/prov';
            InitValue = ' ';
            TableRelation = if (TipoPersona = const (Cliente)) Customer."No."
            else
            Vendor."No.";

            trigger OnValidate()
            begin
                if not (TipoPersona in [TipoPersona::Cliente, TipoPersona::Proveedor]) then begin
                    FieldError(TipoPersona, ' Debe ser cliente o proveedor');
                end;
            end;

            trigger OnLookup()
            var
                rlCustomer: Record Customer;
                rlVendor: Record Vendor;
                plCustomerLookup: Page "Customer Lookup";
                plVendorLookup: Page "Vendor Lookup";

            begin
                case TipoPersona of
                    TipoPersona::Cliente:
                        with rlCustomer do begin
                            plCustomerLookup.GetRecord(rlCustomer);
                            plCustomerLookup.LookupMode(true); // Muestra el boton de aceptar o cancelar
                            plCustomerLookup.VisibilidadCreditoMaximoVisible(false);

                            if Get(Rec.NumPersona) then begin
                            end;

                            if plCustomerLookup.RunModal() in [Action::LookupOK, Action::OK] then begin
                                plCustomerLookup.GetRecord(rlCustomer);
                                Rec.Validate(NumPersona, "No.");
                            end;
                        end;

                    TipoPersona::Proveedor:
                        with rlVendor do begin
                            plVendorLookup.GetRecord(rlVendor);
                            plVendorLookup.LookupMode(true); // Muestra el boton de aceptar o cancelar

                            if Get(Rec.NumPersona) then begin
                            end;

                            if plVendorLookup.RunModal() in [Action::LookupOK, Action::OK] then begin
                                plVendorLookup.GetRecord(rlVendor);
                                Rec.Validate(NumPersona, "No.");
                            end;
                        end;
                end
            end;
        }

        field(8; "SumaImporte"; Decimal)
        {
            Caption = 'Suma Importe';
            FieldClass = FlowField;
            CalcFormula = sum ("Sales Line".Amount where ("Sell-to Customer No." = field (NumCliente),
            "Shipment Date" = field (FiltroFecha), "No." = field (FiltroProducto)));
        }

        field(9; "FiltroFecha"; Date)
        {
            Caption = 'Filtro fecha de envio';
            FieldClass = FlowFilter;
        }

        field(10; FiltroProducto; Code[20])
        {
            Caption = 'Filtro de producto';
            FieldClass = FlowFilter;
            TableRelation = "Item"."No.";
        }

        field(11; "NombreCliente"; Text[2048])
        {
            Caption = 'Nombre cliente';
            FieldClass = FlowField;
            CalcFormula = lookup (Customer.Name where ("No." = field (NumCliente)));
        }

        field(12; "CodAve"; Code[10])
        {
            Caption = 'Codigo Ave';
            TableRelation = TCN_Ave.Codigo;
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(PK; Color)
        {
            Clustered = true;
        }
        key(Secundaria; NumCliente, Color, DiaSemana)
        {

        }
    }

    trigger OnInsert()
    begin
        Message('Registro creado');
    end;

    trigger OnRename()
    begin
        Error('No se permite cambio de clave');
    end;

}